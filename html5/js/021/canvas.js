function draw( id ){
	var canvas = document.getElementById( id );//获取canvas对象
	var context = canvas.getContext( '2d' );//获取上下文
	context.fillStyle = "#000";//货值填充的颜色
	context.strokeStyle = "#fff";//绘制边框的颜色
	context.lineWidth = 5;//设置画笔的宽度为5
	context.fillRect( 0,0,400,300 );//fillRect() 方法绘制“已填色”的矩形。默认的填充颜色是黑色。参数分别为x坐标，y坐标宽，长
	context.strokeRect( 50,50,180,120 );//strokeRect(x,y,width,heigth) 方法绘制矩形（不填色）。笔触的默认颜色是黑色。
	context.strokeStyle = "#f60";//绘制边框的颜色
	context.strokeRect( 110,110,180,120 );//strokeRect(x,y,width,heigth) 方法绘制矩形（不填色）。笔触的默认颜色是黑色。
}