//保存数据方法
function saveStorage( id ){
	var data = document.getElementById( id ).value;
	var time = new Date().getTime();
	localStorage.setItem(time,data);
	alert('success');
}

//读取数据
function loadStorage( id ){
	var result = '<table border="1">'; 
	for( var i=0; i < localStorage.length; i++ ){
		var key = localStorage.key(i);
		var value = localStorage.getItem(key);
		var date = new Date();
		date.setTime(key);
		var datestr = date.toGMTString();
		result += '<tr><td>这是第'+i+'条数据:'+value+'</td></tr>';
	}
	result += '</table>';
	var target = document.getElementById( id );
	target.innerHTML = result;
}

//清除数据
function clearStorage( id ){
	localStorage.clear();
}