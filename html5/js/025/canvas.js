var context;
var width;
var height;
var i;
function draw( id ){
	var canvas = document.getElementById( id );
	context = canvas.getContext("2d");
	width = canvas.width;
	height = canvas.height;
	//绘制画布
	// context.fillStyle = "pink";
	// context.fillRect( 0,0,10,10 );
	setInterval( painting,100 );
	i = 0;
}

//定义绘画函数
function painting(){
	//绘制小矩形
	// context.fillStyle = "green";
	// context.fillRect( i,i,10,10 );
	// context.fillRect( 400-i,400-i,10,10 );
	//绘制矩形,并实现擦除效果
	context.fillStyle = "#fff";
	context.clearRect( 0,0,width,height );
	context.fillStyle = "pink";
	context.fillRect( i,0,10,10 );
	i += 20;
	if( i >= width ){
		i = 0;
	}
}