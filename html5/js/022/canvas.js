function draw( id ){
	var canvas = document.getElementById( id );
	var context = canvas.getContext( "2d" );//获取上下文
	//绘制背景
	context.fillStyle = '#f1f2f3';
	context.fillRect(0,0,400,400);
	//循环生成圆形
	for( var i=0;i<10;i++ ){
		//绘制圆形
		context.beginPath();//创建图形路径
		context.arc( i*25,i*25,i*10,0,Math.PI*2,true );//绘制圆形
		context.closePath();//关闭路径
		//绘制填充
		context.fillStyle = "rgba(255,0,0,0.25)";//填充圆形的颜色
		context.fill();
		//绘制边框
		context.strokeStyle = "red";
		context.stroke();
	}
}